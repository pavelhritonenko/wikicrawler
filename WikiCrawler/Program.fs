﻿open System
open System.Text.RegularExpressions

type Article = 
    { Id : string
      Title : string option
      References : string list }

let client = 
    let c = new System.Net.Http.HttpClient()
    c.BaseAddress <- new Uri("http://en.wikipedia.org/wiki/")
    c

let getHtml (pageId : string) = 
    async { 
        let! response = client.GetAsync(pageId) |> Async.AwaitTask
        match response.IsSuccessStatusCode with
        | false -> return None
        | true -> let! str = response.Content.ReadAsStringAsync() |> Async.AwaitTask
                  return Some str
    }

let title html = 
    let m = Regex.Match(html, """\<title\>(.*) \- Wikipedia\, the free encyclopedia\<\/title\>""")
    match m.Success with
    | false -> None
    | true -> Some(m.Groups.Item(1).Value)

let findReferences html = 
    Regex.Matches(html, """"/wiki/([^:]*?)[\"\#]""")
    |> Seq.cast<Match>
    |> Seq.map (fun x -> x.Groups.Item(1).Value)
    |> Seq.distinct
    |> List.ofSeq

let fetch pageId = 
    async { 
        let! html = getHtml pageId
        match html with
        | None -> return None
        | Some content -> 
            return Some { Id = pageId
                          Title = (title content)
                          References = (findReferences content |> List.filter (fun x -> x <> pageId)) }
    }

type Target = 
    { PageId : string
      Depth : int }

type Command = 
    | Fetch of Target
    | Append of Article
    | Error
    | Stop

type Message = Command * AsyncReplyChannel<Set<Article>>

type SupervisorState = 
    { Nodes : Set<Article>
      Counter : int
      Started : bool }

[<EntryPoint>]
let main argv = 
    let start = 
        { PageId = "Immutable_object"
          Depth = 2 }
    
    let inbox = 
        MailboxProcessor<Message>.Start(fun x -> 
            let rec loop s = 
                async { 
                    let state = { s with Started = true }
                    let! (msg, replyChannel) = x.Receive()
                    if s.Started && state.Counter = 0 && x.CurrentQueueLength = 0 && msg <> Stop then 
                        x.Post(Stop, replyChannel)
                        return! loop state
                    match msg with
                    | Append(a) -> 
                        return! loop { state with Counter = state.Counter - 1
                                                  Nodes = Set.add a state.Nodes }
                    | Stop -> 
                        replyChannel.Reply s.Nodes
                        return ()
                    | Error -> return! loop { state with Counter = state.Counter - 1 }
                    | Fetch(t) -> 
                        if t.Depth = 0 then return! loop state
                        if Set.exists (fun a -> t.PageId = a.Id) state.Nodes then return! loop state
                        async { 
                            let! article = fetch t.PageId
                            match article with
                            | None -> (Error, replyChannel) |> x.Post
                            | Some a -> 
                                (Append a, replyChannel) |> x.Post
                                for refId in a.References do
                                    (Fetch { PageId = refId
                                             Depth = t.Depth - 1 }, replyChannel)
                                    |> x.Post
                        }
                        |> Async.Start
                        return! loop { state with Counter = state.Counter + 1 }
                }
            loop { Nodes = Set.empty<Article>
                   Counter = 0
                   Started = false })
    
    inbox.PostAndReply(fun x -> (Fetch start, x))
    |> Array.ofSeq
    |> printfn "%A"
    0 // return an integer exit code
